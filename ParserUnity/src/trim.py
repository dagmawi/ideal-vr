# import sys, json; 

# print json.load(sys.stdin)["text"]

# with open('VeteransDay-shaun.json') as json_data:
#     data = json.load(json_data)
#     for element in data: 
#         del element['created_at'] 


import sys
import  json


def main(args):
    try:
        inputFile = open(args[1])
        input = json.load(inputFile)
        inputFile.close()
    except IndexError:
        usage()
        return False
    if len(args) < 3:
        print json.dumps(input, sort_keys = False, indent = 4)
    else:
        outputFile = open(args[2], "w")
        json.dump(input, outputFile, sort_keys = False, indent = 4)
        outputFile.close()
    return True


def usage():
    print __doc__


if __name__ == "__main__":
    sys.exit(not main(sys.argv))
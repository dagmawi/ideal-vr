import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;


public class ParserCategories {

	//stores tweets for collection currently being examined
	private static ArrayList<String> tweets;
	private static int K = 22;
	private static int[] occurenceK = new int[K];
	
	public static void main(String[] args) throws IOException {

		//array for collection category names such as Community
		//or Shootings to be stored
		ArrayList<File> categories = new ArrayList<File>();
		//Text file where this parser will output the JSON string
		//for the category view scene
		File file = new File("StreamingAssets/inputJSON.txt");
	    FileWriter writer = new FileWriter(file); 
		StringBuilder bob = new StringBuilder();

		//gets category names from the directory below
		listCategories("StreamingAssets", categories);
		bob.append("[");
		
		//processes each subdirectory (or category) to create correct
		//JSON string then adds to stringbuilder
		for(int i = 0; i < categories.size(); i++){
			bob.append(parseCategories(categories.get(i).toString()));
			bob.append(",");
		}
		bob.deleteCharAt(bob.length()-1);
		bob.append("]");
		
		// creates the file
	    file.createNewFile();
	    // creates a FileWriter Object
	    // Writes the content to the file
	    writer.write(bob.toString()); 
	    writer.flush();
	    writer.close();
	}
	
	public static void listCategories(String directoryName, ArrayList<File> files) {
	    File directory = new File(directoryName);

	    // get all the files from a directory
	    File[] fList = directory.listFiles();
	    for (File file : fList) {
	        if (file.isDirectory()) {
	            files.add(file);
	        }
	    }
	}
	
	public static void listTweetCollections(String directoryName, ArrayList<File> files) {
	    File directory = new File(directoryName);

	    // get all the files from a directory
	    File[] fList = directory.listFiles();
	    for (File file : fList) {
	        if (file.isFile()) {
	            files.add(file);
	        }
	    }
	}
	public static String parseCategories(String directoryName) throws IOException {
	    File directory = new File(directoryName);
	    //stores initial collections with spaces between every entry
	    List<String> collectionSpaced;
	    //new list without spaces between entries
	    List<String> collectionStrings;
	    //the number of lines in a category (aka the number of tweets contained)
	    int categorySize = 0;
	    //directory/category file where JSON string will be stored
		File openDir;
		//writes to the category file
	    FileWriter writer;
	    
		StringBuilder bob = new StringBuilder();
		
		//folder name of current category
		String folderName = directoryName.substring(directoryName.length() - directoryName.substring(16).length());
		bob.append(directoryName.substring(0, 15));
		bob.append("/");
		bob.append(folderName);
		bob.append("/");
		bob.append(folderName);
		bob.append(".txt");
		
		String categoryFilename = bob.toString();
		
		//creates a new file in the current category folder named [category].txt
		openDir = new File(categoryFilename);
		//opens the file
		writer = new FileWriter(openDir); 
			
		bob = new StringBuilder();

	    // get all the files from a directory
	    File[] fList = directory.listFiles();
	    bob.append("[");
	    for (File file : fList) {
	        if (file.isFile()) {
	    		//strips off .json footer tag
	    		String parseName = file.toString().substring(0, file.toString().length() - 5);
	    		//strips off beginning directory path to extract name of tweet collection
	    		String tweetName = parseName.substring(directoryName.length() + 1, parseName.length());
	    		
	    		//reset tweets for this collection
	    		tweets = new ArrayList<String>();
	    		
	    		collectionSpaced = Files.readAllLines(file.toPath(), Charset.defaultCharset());
	    		collectionStrings = new ArrayList<String>();
	    		String currLine;
	    		for(int i = 0; i < collectionSpaced.size(); i+=2){
	    			currLine = collectionSpaced.get(i);
	    			collectionStrings.add(currLine);
	    			parseTweet(currLine);
	    		}
	    		
	    		categorySize += collectionStrings.size();
	    		
	    		//to account for empty file that we are going to store this JSON string in;
	    		//we don't want to make a JSON string for this file
	    		if(collectionStrings.size() > 0){
		    	    bob.append(outputJSON(tweetName, collectionStrings.size()));
		    	    bob.append(",");
	    		}
	        }

	    }
	    String tweetsNoPunc = tweets.toString().replaceAll("[.,:]", "");
	    String tweetsNoDash = tweetsNoPunc.replaceAll("(^|\\s+)-(\\s+|$)", "");
	    String tweetsNoExtraSpaces = tweetsNoDash.replaceAll(" +", " ");
	    String[] frequentWords = topKWordsSelect( tweetsNoExtraSpaces , K);
	    
	    for(int i = 0; i < frequentWords.length; i++){
	        System.out.print(frequentWords[i] + "\n");
	    }
	    
	    System.out.println("-------------------------------------------------------------------------------------------------------------------");
	            
		bob.deleteCharAt(bob.length()-1);
		bob.append("]");
		
		writer.write(bob.toString());
	    writer.flush();
	    writer.close();

	    return outputJSON(directoryName, categorySize);
	    
	}
	
	public static String[] topKWordsSelect(final String stream, final int k) throws IOException {
	    final Map<String, Integer> frequencyMap = new HashMap<String, Integer>();
	    
	    ArrayList<String> stopWords = new ArrayList<String>();
	    
	    //our default stopwords
	    stopWords.add("https");
	    stopWords.add("//t");
	    stopWords.add("rt");
	    stopWords.add("-");
	    stopWords.add("�");	
	    stopWords.add("&amp;");
	    stopWords.add("https�");
	    
	    //stopwords obtained online
	    readStopWords(stopWords);


	    final String[] words = stream.toLowerCase().split(" ");
	    for (final String word : words) {
	    	
	    	if(!stopWords.contains(word)){
	        	
		        int freq = 1;
		        if (frequencyMap.containsKey(word)) {
		            freq = frequencyMap.get(word) + 1;
		        }
	
		        // update the frequency map
		        frequencyMap.put(word, freq);
	    	}
	    }

	    // Find kth largest frequency which is same as (n-k)th smallest frequency
	    final int[] frequencies = new int[frequencyMap.size()];
	    int i = 0;
	    for (final int value : frequencyMap.values()) {
	        frequencies[i++] = value;
	    }
	    final int kthLargestFreq = kthSmallest(frequencies, 0, i - 1, i - k);

	    // extract the top K
	    final String[] topK = new String[k];
	    i = 0;
	    for (final java.util.Map.Entry<String, Integer> entry : frequencyMap.entrySet()) {
	        if (entry.getValue().intValue() >= kthLargestFreq) {
	        	//occurrences is entry.getValue()
	            topK[i] = entry.getKey();
	            occurenceK[i++] = entry.getValue().intValue();
	            if (i == k) {
	                break;
	            }
	        }
	    }

	    return topK;
	}
	
	private static void readStopWords(ArrayList<String> stopwords) throws IOException {
		File defaultFile = new File("Stopwords/Default English Stopwords.txt");
		//File longFile = new File("Stopwords/Long List of Stopwords.txt");
		List<String> stopwordsToAdd = Files.readAllLines(defaultFile.toPath(), Charset.defaultCharset());
		stopwords.addAll(stopwordsToAdd);
	}

	private static void swap(final int input[], final int i, final int j) {
	    final int temp = input[i];
	    input[i] = input[j];
	    input[j] = temp;
	}

	private static int partition(final int[] A, final int p, final int r) {
	    final double pivot = A[r];
	    int i = p - 1;
	    int j = p;

	    for (j = p; j < r; j++) {
	        if (A[j] <= pivot) {
	            swap(A, ++i, j);
	        }
	    }

	    swap(A, i + 1, r);
	    return i + 1;
	}

	private static int RandomizedPartition(final int[] A, final int p, final int r) {
	    final int i = (int) Math.round(p + Math.random() * (r - p));
	    swap(A, i, r);
	    return partition(A, p, r);
	}

	public static int kthSmallest(final int[] A, final int p, final int r, final int k) {
	    if (p < r) {
	        final int q = RandomizedPartition(A, p, r);

	        final int n = q - p + 1;
	        if (k == n) {
	            return A[q];
	        } else if (k < n) {
	            return kthSmallest(A, p, q - 1, k);
	        } else {
	            return kthSmallest(A, q + 1, r, k - n);
	        }
	    } else {
	        return Integer.MIN_VALUE;
	    }
	}
	
	public static void parseTweet(String jsonString){
		JSONArray aryJSONStrings = new JSONArray("[" + jsonString + "]");
		String currTweet;
		for (int i=0; i<aryJSONStrings.length(); i++) {
			currTweet = aryJSONStrings.getJSONObject(i).getString("text");
			tweets.add(currTweet);
		}
	}
	
	
	public static String outputJSON(String word, int occurences){
	    StringBuilder bob = new StringBuilder();
	    bob.append("{");
	    bob.append("\"");
	    bob.append("term");
	    bob.append("\"");
	    bob.append(":");
	    bob.append("\"");
	    bob.append(word.substring(word.indexOf("\\") + 1));
	    bob.append("\"");
	    bob.append(", ");
	    bob.append("\"");
	    bob.append("occurrences");
	    bob.append("\"");
	    bob.append(":");
	    bob.append(occurences);
	    bob.append("}");
		return bob.toString();
	}

}

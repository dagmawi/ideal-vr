﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using LitJson;
using System.IO;

public class Phrase
{
    public string term;
    public float occurrences;
}

public class FormWordCloud : MonoBehaviour
{
    public GameObject childObject;
    public float size = 10.0f;
    private List<Phrase> phrases = new List<Phrase>();
    private List<Phrase> randomisedPhrases = new List<Phrase>();
	private List<GameObject> objects = new List<GameObject>();
	private int currentTweetIndex = 0;

    private float totalOccurances = 0.0f;

    private string jsonString;

    void Start()
    {
        getJSON();
        ProcessWords(jsonString);
        Sphere();
    }

	public void updateJSON(string category){
		foreach (GameObject tObject in objects)
        {
            Destroy(tObject, 0.0f);
        }	
		
		phrases.Clear();
		randomisedPhrases.Clear();
		string dbPath = "";
		string file = category + ".txt";
		if (Application.platform == RuntimePlatform.Android)
		{
		  // Android
		  string oriPath = System.IO.Path.Combine(Application.streamingAssetsPath, category);
		  oriPath = System.IO.Path.Combine(oriPath, file);
		  
		  // Android only use WWW to read file
		  WWW reader = new WWW(oriPath);
		  while ( ! reader.isDone) {}
		  
		  string realPath = Application.persistentDataPath + "/db";
		  System.IO.File.WriteAllBytes(realPath, reader.bytes);
		  
		  dbPath = realPath;
		  jsonString = System.IO.File.ReadAllText(dbPath);
		}
		else
		{
		  // Windows
		  dbPath = System.IO.Path.Combine(Application.streamingAssetsPath, category);
		  dbPath = System.IO.Path.Combine(dbPath, file);
		  jsonString = System.IO.File.ReadAllText(dbPath);
		}
		
		ProcessWords(jsonString);
        Sphere();
	}
	
    private void getJSON()
    {
		//jsonString = "[{\"term\":\"the\", \"occurrences\":504},{\"term\":\"to\",\"occurrences\":447},{\"term\":\"rt\",\"occurrences\":433},{\"term\":\"a\",\"occurrences\":382},{\"term\":\"in\",\"occurrences\":299},{\"term\":\"of\",\"occurrences\":274},{\"term\":\"adventure\",\"occurrences\":236},{\"term\":\"and\",\"occurrences\":216},{\"term\":\"for\",\"occurrences\":166},{\"term\":\"is\",\"occurrences\":157},{\"term\":\"on\",\"occurrences\":154},{\"term\":\"cars\",\"occurrences\":136},{\"term\":\"it\",\"occurrences\":122},{\"term\":\"you\",\"occurrences\":116},{\"term\":\"with\",\"occurrences\":100},{\"term\":\"from\",\"occurrences\":87},{\"term\":\"at\",\"occurrences\":85},{\"term\":\"i\",\"occurrences\":85},{\"term\":\"this\",\"occurrences\":85},{\"term\":\"that\",\"occurrences\":83}]";
		
		string dbPath = "";

		if (Application.platform == RuntimePlatform.Android)
		{
		  // Android
		  string oriPath = System.IO.Path.Combine(Application.streamingAssetsPath, "inputJSON.txt");
		  
		  // Android only use WWW to read file
		  WWW reader = new WWW(oriPath);
		  while ( ! reader.isDone) {}
		  
		  string realPath = Application.persistentDataPath + "/db";
		  System.IO.File.WriteAllBytes(realPath, reader.bytes);
		  
		  dbPath = realPath;
		  jsonString = System.IO.File.ReadAllText(dbPath);
		}
		else
		{
		  // Windows
		  dbPath = System.IO.Path.Combine(Application.streamingAssetsPath, "inputJSON.txt");
		  jsonString = System.IO.File.ReadAllText(dbPath);
		}
    }
	
	private void ListView()
    {
        float points = 12;
        float startY = 60;
        for (float i = 0; i < points; i++)
        {
          
            Vector3 pos = new Vector3(startY - (i*10),  0, -20);

            // Create the object as a child of the sphere
            GameObject child = Instantiate(childObject, pos, Quaternion.identity) as GameObject;
            child.transform.parent = transform;
            TextMesh phraseText = child.transform.GetComponent<TextMesh>();
			float scale = 0f;
            
			if(i == 0){
				phraseText.text = "Back";
				
				scale = 50f;
			}else if(i == points - 1){
				phraseText.text = "Forward";
				
				scale = 50f;
			}else{
				Phrase phrase = randomisedPhrases[(((int)i) - 1) + currentTweetIndex];
			
				phraseText.text = phrase.term;

				scale = (phrase.occurrences / totalOccurances) * 10.0f;
			}
            child.transform.localScale = new Vector3(scale, scale, scale);
			objects.Add(child);
        }
    }

    // Update is called once per frame
    void Update()
    {
        Transform camera = Camera.main.transform;

        // Tell each of the objects to look at the camera
        foreach (Transform child in transform)
        {
            child.LookAt(camera.position);
            child.Rotate(0.0f, 180.0f, 0.0f);
        }
    }

    private void Sphere()
    {
        float points = phrases.Count;
        float increment = Mathf.PI * (3 - Mathf.Sqrt(5));
        float offset = 2 / points;
        for (float i = 0; i < points; i++)
        {
            float y = i * offset - 1 + (offset / 2);
            float radius = Mathf.Sqrt(1 - y * y);
            float angle = i * increment;
            Vector3 pos = new Vector3((Mathf.Cos(angle) * radius * size), y * size, Mathf.Sin(angle) * radius * size);

            // Create the object as a child of the sphere
            GameObject child = Instantiate(childObject, pos, Quaternion.identity) as GameObject;
            child.transform.parent = transform;
            TextMesh phraseText = child.transform.GetComponent<TextMesh>();

            Phrase phrase = randomisedPhrases[(int)i];
            phraseText.text = phrase.term;

            float scale = (phrase.occurrences / totalOccurances) * 10.0f;
            child.transform.localScale = new Vector3(scale, scale, scale);
			objects.Add(child);
        }
    }

    private void ProcessWords(string jsonString)
    {
        totalOccurances = 0.0f;

        JsonData jsonvale = JsonMapper.ToObject(jsonString);
        for (int i = 0; i < jsonvale.Count; i++)
        {
            Phrase phrase = new Phrase();
            phrase.term = jsonvale[i]["term"].ToString();
            phrase.occurrences = float.Parse(jsonvale[i]["occurrences"].ToString());
            phrases.Add(phrase);
            totalOccurances += phrase.occurrences;
        }

        System.Random random = new System.Random();
        randomisedPhrases.Clear();
        for (int i = 0; i < phrases.Count; i++)
        {
            randomisedPhrases.Add(phrases[i]);
        }

        for (int i = 0; i < randomisedPhrases.Count; i++)
        {
            int first = i;
            int second = random.Next(0, randomisedPhrases.Count);
            Phrase temp = randomisedPhrases[second];
            randomisedPhrases[second] = randomisedPhrases[first];
            randomisedPhrases[first] = temp;
        }
    }
}
